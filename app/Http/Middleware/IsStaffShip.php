<?php

namespace App\Http\Middleware;

use Closure;

class IsStaffShip
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->staffShip) {
            return $next($request);
        }

        return redirect('/');
    }
}
