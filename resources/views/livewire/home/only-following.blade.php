<span class="float-end">
    <div class="form-check">
        <input wire:click="onlyFollowingsTasks" class="form-check-input" type="checkbox" id="onlyFollowingsTasks" {{ auth()->user()->onlyFollowingsTasks ? 'checked' : '' }}>
        <label class="form-check-label" for="onlyFollowingsTasks">
            Only following
        </label>
        <div wire:loading class="spinner-border taskord-spinner spinner-border-sm ms-1"></div>
    </div>
</span>
