<div class="mt-3">
    <button wire:loading.attr="disabled" wire:click="loadMore" class="btn w-100 btn-primary mb-4" id="load-more">Load More</button>
</div>
