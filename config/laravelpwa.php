<?php

return [
    'name' => 'Taskord',
    'manifest' => [
        'name' => 'Taskord',
        'short_name' => 'Taskord',
        'start_url' => '/',
        'background_color' => '#6a63ec',
        'theme_color' => '#6a63ec',
        'display' => 'standalone',
        'orientation'=> 'any',
        'status_bar'=> 'black',
        'icons' => [
            '72x72' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-72x72_9XXQQ8rO49.png',
                'purpose' => 'any',
            ],
            '96x96' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-96x96_iGOfQtMPUa.png',
                'purpose' => 'any',
            ],
            '128x128' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-128x128_jWfaVdJko.png',
                'purpose' => 'any',
            ],
            '144x144' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-144x144_QOCTg3a3oS.png',
                'purpose' => 'any',
            ],
            '152x152' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-152x152_3jjO5R_cz.png',
                'purpose' => 'any',
            ],
            '192x192' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-192x192_jTC0KcZevQ.png',
                'purpose' => 'any',
            ],
            '384x384' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-384x384_GPM-rSoww.png',
                'purpose' => 'any',
            ],
            '512x512' => [
                'path' => 'https://ik.imagekit.io/taskordimg/seo/icon-512x512_wXNDRqY6nz.png',
                'purpose' => 'any',
            ],
        ],
        'splash' => [
            '640x1136' => 'https://ik.imagekit.io/taskordimg/seo/splash-640x1136_jQfG9_VWt2.png',
            '750x1334' => 'https://ik.imagekit.io/taskordimg/seo/splash-750x1334_tQe5OWdrk.png',
            '828x1792' => 'https://ik.imagekit.io/taskordimg/seo/splash-828x1792_XX3nM-b_va.png',
            '1125x2436' => 'https://ik.imagekit.io/taskordimg/seo/splash-1125x2436_diVeRRP5JE.png',
            '1242x2208' => 'https://ik.imagekit.io/taskordimg/seo/splash-1242x2208_P6W0bB7SR.png',
            '1242x2688' => 'https://ik.imagekit.io/taskordimg/seo/splash-1242x2688_yI7dFlHQq5.png',
            '1536x2048' => 'https://ik.imagekit.io/taskordimg/seo/splash-1536x2048_PoTOATFGHo.png',
            '1668x2224' => 'https://ik.imagekit.io/taskordimg/seo/splash-1668x2224_UXoDPCWgMQ.png',
            '1668x2388' => 'https://ik.imagekit.io/taskordimg/seo/splash-1668x2388_egSQN_GVt.png',
            '2048x2732' => 'https://ik.imagekit.io/taskordimg/seo/splash-2048x2732_Zq9t2Bd_Df.png',
        ],
        'shortcuts' => [],
        'custom' => [],
    ],
];
